package com.example.demo.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.doAnswer;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.example.demo.entity.CartProductEntity;
import com.example.demo.entity.OrderDataEntity;
import com.example.demo.entity.OrderEntity;
import com.example.demo.entity.UserEntity;
import com.example.demo.impl.UserImpl;
import com.example.demo.service.CartService;
import com.example.demo.service.OrderService;
import com.example.demo.testUtills.TestUtills;
/**
 * Test case related to Order item
 * 
 * @author PawanTripathi
 * 
 */
public class OrderControllerTest extends TestUtills {
	/*
	 * @Autowired CartController cartController;
	 */

	@MockBean
	UserImpl userImpl;
	
	@MockBean
	OrderService orderService;
	@MockBean
	CartService cartService;

	
	
	@BeforeEach
	public void setUp() {
		super.setUp();
	}

	/**
	 * test add Submit Order
	 */
	@Test
	public void submitOrder() {
			try {
				
	         UserEntity userRequest = new UserEntity();
				userRequest.setName("Pawan");
				userRequest.setId(1L);
				userRequest.setAddress("Hcl Noida");
		        Mockito.when(userImpl.addUser(userRequest)).thenReturn(userRequest);
				CartProductEntity cartRequest = new CartProductEntity();
				cartRequest.setProductname("Vovo v 10");
				cartRequest.setPrice(90000.00);
				cartRequest.setId(1L);
				cartRequest.setProductquantity(15.00);
				
				doAnswer((i) -> {
		            return null;
		        }).when(cartService).addtoCart(cartRequest);;
				RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/submitOrder/1")
						.accept(MediaType.APPLICATION_JSON)
						.characterEncoding("utf-8");
				MvcResult result = mvc.perform(requestBuilder).andReturn();
				int status1 = result.getResponse().getStatus();
				String content1 = result.getResponse().getContentAsString();
				System.out.println(content1);
				assertEquals(status1, 201);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}	
	}
	
	/**
	 * test case for   getOrderbyId
	 */
	@Test
	public void getOrderbyId() {
			try {
				
	         UserEntity userRequest = new UserEntity();
				userRequest.setName("Pawan");
				userRequest.setId(1L);
				userRequest.setAddress("Hcl Noida");
		        
				OrderDataEntity orderDataEntity=new OrderDataEntity();
		    	Set<OrderDataEntity> items=new HashSet<>();
				OrderEntity orderentity=new OrderEntity();
				orderentity.setId(2L);
				orderentity.setItems(items);
				orderDataEntity.setId(2L);
				orderDataEntity.setPrice(1000.00);
				orderDataEntity.setProductname("Vovo v 10");
				orderDataEntity.setProductquantity(1.00);
				orderDataEntity.setUser(userRequest);
				orderDataEntity.setOrderEntity(orderentity);
				items.add(orderDataEntity);
				
				
				List<OrderDataEntity> orders = new ArrayList<>();
				
				orders.add(orderDataEntity);
				
				  Mockito.when(orderService.getSingleOrder("101")).thenReturn(orders);
				
				
				RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/getOrder/2")
						.accept(MediaType.APPLICATION_JSON)
						.characterEncoding("utf-8");
				MvcResult result = mvc.perform(requestBuilder).andReturn();
				int status1 = result.getResponse().getStatus();
				String content1 = result.getResponse().getContentAsString();
				System.out.println(content1);
				assertEquals(status1,201 );
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}	
	}
	
	
	/**
	 * test case for   Update Address
	 */
	
	@Test
	public void updateAddress() {
			try {
				

		        doAnswer((i) -> {
		            return null;
		        }).when(orderService).updateAddress("5", "Noida HCL");
				
				

			    MvcResult mvcResult = mvc.perform(
						MockMvcRequestBuilders.post("/updateaddress?order_id=5&address=dsffdfd").accept(MediaType.APPLICATION_JSON)
						.contentType(MediaType.APPLICATION_JSON_VALUE))
						.andReturn();
			    
			    
				int status1 = mvcResult.getResponse().getStatus();
				
				System.out.println("updateaddresscontent"+mvcResult);
				assertEquals(200, status1);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}	
	}
	
	/**
	 * test case for   Update Order Quantity
	 */
	
	
	@Test
	public void shortData() {
			try {
				

		        doAnswer((i) -> {
		            return null;
		        }).when(orderService).sortType("price");
				
				

			    MvcResult mvcResult = mvc.perform(
						MockMvcRequestBuilders.get("/getshorteddata/{sort_type}").accept(MediaType.APPLICATION_JSON)
						.contentType(MediaType.APPLICATION_JSON_VALUE))
						.andReturn();
			    
			    
				int status1 = mvcResult.getResponse().getStatus();
				
				assertEquals(200, status1);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}	
	}
	
	
	/**
	 * test case for   Update Order Quantity
	 */
	
	@Test
	public void updateQuantity() {
			try {
				

		        doAnswer((i) -> {
		            return null;
		        }).when(orderService).updateOrderQuanity("1", "1", "1", "1");
				
				

			    MvcResult mvcResult = mvc.perform(
						MockMvcRequestBuilders.post("/updateQuantity?order_id=1&prouct_id=1&quanitity=2&user_id=1").accept(MediaType.APPLICATION_JSON)
						.contentType(MediaType.APPLICATION_JSON_VALUE))
						.andReturn();
			    
			    
				int status1 = mvcResult.getResponse().getStatus();
				
				assertEquals(200, status1);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}	
	}
	
	/**
	 * test case for   Update Order Status
	 */
	
	@Test
	public void updateOrderStatus() {
			try {
				

		        doAnswer((i) -> {
		            return null;
		        }).when(orderService).updateOrderStatus("5", "Complted");
			    MvcResult mvcResult = mvc.perform(
						MockMvcRequestBuilders.post("/updateOrderstatus?order_id=5&status=Complted").accept(MediaType.APPLICATION_JSON)
						.contentType(MediaType.APPLICATION_JSON_VALUE))
						.andReturn();
			    
			    
				int status1 = mvcResult.getResponse().getStatus();
				
				assertEquals(200, status1);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}	
	}
	
	/**
	 * test case for Cancel Order 
	 */
	
	@Test
	public void cancelOrder() {
			try {
				

		        doAnswer((i) -> {
		            return null;
		        }).when(orderService).cancelOrder("5", "Cancelled");
				
				

			    MvcResult mvcResult = mvc.perform(
						MockMvcRequestBuilders.post("/cancelOrder?order_id=5&status=cancelled").accept(MediaType.APPLICATION_JSON)
						.contentType(MediaType.APPLICATION_JSON_VALUE))
						.andReturn();
			    
			    
				int status1 = mvcResult.getResponse().getStatus();
				
				System.out.println("updateaddresscontent"+mvcResult);
				assertEquals(200, status1);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}	
	}
	
	
}
