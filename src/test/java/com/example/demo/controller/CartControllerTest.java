package com.example.demo.controller;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.doAnswer;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import com.example.demo.entity.CartProductEntity;
import com.example.demo.entity.UserEntity;
import com.example.demo.impl.UserImpl;
import com.example.demo.service.CartService;
import com.example.demo.service.OrderService;
import com.example.demo.testUtills.TestUtills;

/**
 * Test case related to cart item
 * 
 * @author PawanTripathi
 * 
 */
@RunWith(SpringRunner.class)
@SpringBootTest
class CartControllerTest extends TestUtills {

	
	
	/*
	 * @Autowired CartController cartController;
	 */
	/*
	 * @Mock  UserImpl;
	 */
	@MockBean
	UserImpl userImpl;
	/*
	 * @Mock  OrderService;
	 */
	@MockBean
	OrderService orderService;
	/*
	 * @Mock  CartService;
	 */
	@MockBean
	CartService cartService;
	/*
	 * @setup controller  CartService;
	 */
	@BeforeEach
	public void setUp() {
		super.setUp();
	}

	/**
	 * test add product in cart
	 */
	@Test
	public void testAddToCart() {
		String uri = "/addtocart";
		try {
			 UserEntity userRequest = new UserEntity();
				userRequest.setName("Pawan");
				userRequest.setId(1L);
				userRequest.setAddress("Hcl Noida");
		        Mockito.when(userImpl.addUser(userRequest)).thenReturn(userRequest);
				CartProductEntity cartRequest = new CartProductEntity();
				cartRequest.setProductname("Vovo v 10");
				cartRequest.setPrice(90000.00);
				cartRequest.setId(1L);
				cartRequest.setProductquantity(15.00);
				String inputJson = super.mapToJson(cartRequest);
				doAnswer((i) -> {
		            return null;
		        }).when(cartService).addtoCart(cartRequest);;
		        MvcResult mvcResult = mvc.perform(
						MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson))
						.andReturn();
				int status1 = mvcResult.getResponse().getStatus();
				String content1 = mvcResult.getResponse().getContentAsString();
				System.out.println(content1);
				assertEquals(status1,201 );
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		}
	}

}
