/**
 * 
 */
package com.example.demo.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.example.demo.entity.ProductEntity;
import com.example.demo.testUtills.TestUtills;

/**
 * test for add product in product table
 * 
 * @author Pawan Tripathi
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest
class ProductControllerTest extends TestUtills {

	/*
	 * @Autowired private ProductController productController;
	 */
	@BeforeEach
	public void setUp() {
		super.setUp();
	}

	/**
	 * test for add product in table
	 */
	@Test
	public void testAddProduct() {

		String uri = "/addproduct";
		try {
			ProductEntity  prodReq = new ProductEntity();
						prodReq.setProductname("Vovo v 10");
						prodReq.setId(1L);
			prodReq.setPrice(90000.00);
			prodReq.setProductquantity(2.00);
			String inputJson = super.mapToJson(prodReq);
			MvcResult mvcResult = mvc.perform(
					MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson))
					.andReturn();
			int status = mvcResult.getResponse().getStatus();
			assertEquals(status, 201);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
