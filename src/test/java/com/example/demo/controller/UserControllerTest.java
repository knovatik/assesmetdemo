/**
 * 
 */
package com.example.demo.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.example.demo.entity.UserEntity;
import com.example.demo.testUtills.TestUtills;
import com.online.ecommarce.model.UserRequest;

/**
 * Test case related to user
 * 
 * @author Pawan Kumar
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest
class UserControllerTest extends TestUtills {

	/*
	 * @Autowired private UserController userController;
	 */

	@BeforeEach
	public void setUp() {
		super.setUp();
	}

	/**
	 * test user registration
	 */
	@Test
	public void testUserRegistration() {

		String uri = "/registeruser";
		try {
			UserEntity userRequest = new UserEntity();
			
			userRequest.setName("Pawan");
			userRequest.setAddress("Hcl Noida");
			userRequest.setEmail("Pawan Tripathi986@gmail.com");
			String inputJson = super.mapToJson(userRequest);
			MvcResult mvcResult = mvc.perform(
					MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson))
					.andReturn();

			int status = mvcResult.getResponse().getStatus();
			assertEquals(status, 200);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
