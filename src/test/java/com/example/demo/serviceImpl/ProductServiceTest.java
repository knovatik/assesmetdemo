package com.example.demo.serviceImpl;

import static org.mockito.Mockito.when;

import org.aspectj.lang.annotation.Before;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.demo.entity.ProductEntity;
import com.example.demo.repository.ProductRepository;
import com.example.demo.service.ProductService;

/**
 * test for  Product Service
 * 
 * @author Pawan Tripathi
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class ProductServiceTest {
	@InjectMocks
	private ProductService productService;
	@Mock
	private ProductRepository productRepository;
	
	@Before(value = "")
	public void init() {
		MockitoAnnotations.initMocks(this);
	} 
	/**
	 * test case for add Product
	 */
	@Test
	public void addProduct() {
		ProductEntity productEntity = new ProductEntity();
		productEntity.setId(2l);
		productEntity.setPrice(1000.00);
		productEntity.setProductname("Vivo v5 ");
		when(productRepository.save(Mockito.any())).thenReturn(productEntity);
	
		Assert.assertEquals(productService.addProduct(productEntity).getPrice(), productEntity.getPrice());
	}
	
	

}
