package com.example.demo.serviceImpl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.demo.entity.CartEntity;
import com.example.demo.entity.CartProductEntity;
import com.example.demo.entity.OrderDataEntity;
import com.example.demo.entity.OrderEntity;
import com.example.demo.entity.UserEntity;
import com.example.demo.repository.CartProductRepository;
import com.example.demo.repository.CartRepository;
import com.example.demo.repository.OrderDataRepository;
import com.example.demo.repository.OrderRepository;
import com.example.demo.repository.UserRepository;
import com.example.demo.service.OrderService;
/**
 * test for add Order Service
 * 
 * @author Pawan Tripathi
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class OrderServiceTest {
	@InjectMocks
	private OrderService orderService;
	@Mock
	private UserRepository userRepository;
	@Mock
	private CartRepository cartRepository;
	@Mock
	private CartProductRepository cartProductrepo;
	@Mock
	private OrderDataRepository orderDataRepository;

	@Mock
	private OrderRepository orderRepository;

	@Mock
	List<CartProductEntity> cartProductEntityList;

	@Mock
	List<OrderDataEntity> orderDataEntities = new ArrayList<>();

	@Mock
	JdbcTemplate jdbcTemplate;
	@Mock
	List<OrderDataEntity> orders=new ArrayList<>();
	@Mock
	List<Map<String, Object>> orderDataEntity=new ArrayList<>();

	@Test
	public void testSubmitOrder() {
		UserEntity userEntity = new UserEntity();
		CartEntity cartEntity = new CartEntity();
		cartEntity.setCatrtid(2l);
		cartEntity.setCattotal(2000L);

		userEntity.setName("Pawan");
		userEntity.setAddress("Hcl Noida");
		userEntity.setEmail("PawanKumar.Pa@hcl.com");
		cartEntity.setUser(userEntity);
		CartProductEntity cartRequest = new CartProductEntity();
		cartRequest.setProductname("Vovo v 10");
		cartRequest.setPrice(90000.00);
		cartRequest.setId(1L);
		cartRequest.setProductquantity(15.00);
		Optional<UserEntity> userOptional = Optional.of(userEntity);
		Optional<CartEntity> cartOptional = Optional.of(cartEntity);
		OrderDataEntity orderDataEntity = new OrderDataEntity();
		Set<OrderDataEntity> items = new HashSet<>();
		OrderEntity orderentity = new OrderEntity();
		orderentity.setId(2L);
		orderentity.setItems(items);
		orderDataEntity.setId(2L);
		orderDataEntity.setPrice(1000.00);
		orderDataEntity.setProductname("Vovo v 10");
		orderDataEntity.setProductquantity(1.00);
		orderDataEntity.setUser(userEntity);
		orderDataEntity.setOrderEntity(orderentity);
		items.add(orderDataEntity);
		cartProductEntityList = new ArrayList<>();
		when(userRepository.findById(Mockito.any())).thenReturn(userOptional);
		when(cartRepository.findById(Mockito.any())).thenReturn(cartOptional);
		when(cartProductrepo.findAll()).thenReturn(cartProductEntityList);
		doAnswer((i) -> {
			return orderDataEntity;
		}).when(orderDataRepository).save(orderDataEntity);
		doAnswer((i) -> {
			return orderDataEntities;
		}).when(orderDataRepository).saveAll(orderDataEntities);

		doAnswer((i) -> {
			return 4;
		}).when(cartRepository).deleteById(cartRequest.getId());

		assertEquals(orderService.submitOrder(2L).size(), orderDataEntities.size());
	}

	@Test
	public void TestUpdateAddress() {
		OrderDataEntity orderDataEntity = new OrderDataEntity();
		Set<OrderDataEntity> items = new HashSet<>();
		
		UserEntity userEntity = new UserEntity();
		CartEntity cartEntity = new CartEntity();
		cartEntity.setCatrtid(2l);
		cartEntity.setCattotal(2000L);

		userEntity.setName("Pawan");
		userEntity.setAddress("Hcl Noida");
		userEntity.setEmail("PawanKumar.Pa@hcl.com");
		cartEntity.setUser(userEntity);
		OrderEntity orderentity = new OrderEntity();
		orderentity.setId(2L);
		orderentity.setItems(items);
		orderDataEntity.setId(2L);
		orderDataEntity.setPrice(1000.00);
		orderDataEntity.setProductname("Vovo v 10");
		orderDataEntity.setProductquantity(1.00);
		orderDataEntity.setUser(userEntity);
		orderDataEntity.setOrderEntity(orderentity);
		items.add(orderDataEntity);
		Optional<OrderEntity> orderOptional = Optional.of(orderentity);
		when(orderRepository.findById(Mockito.any())).thenReturn(orderOptional);

	}
	
	
	
	@Test
	public void testgetOrderByID() {
		String sql = "SELECT * FROM ORDER_DATA_ENTITY where ORDERNEW_ID=?";
		when(jdbcTemplate.queryForList(sql, new Object[]{1})).thenReturn(orderDataEntity);
		
		assertEquals(orderService.getSingleOrder("1").size(), orders.size());

	}

}
