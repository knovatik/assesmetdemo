package com.example.demo.serviceImpl;

import static org.mockito.Mockito.when;

import org.aspectj.lang.annotation.Before;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.demo.entity.UserEntity;
import com.example.demo.impl.UserImpl;

/**
 * test UserImpl action
 * @author Pawan Kumar
 * 
 */

@RunWith(SpringRunner.class)
@SpringBootTest
class UserImplTest {
	@InjectMocks
	private UserImpl mockUserImpl;
	@Mock
	private com.example.demo.repository.UserRepository userReposiotry;
	

	@Before(value = "")
	public void init() {
		MockitoAnnotations.initMocks(this);
	} 
	
	

	
	
	/**
	 * test case for user registration
	 */
	@Test
	public void testUserRegistation() {
		UserEntity userInfo = new UserEntity();
		UserEntity userEntity = new UserEntity();
		userEntity.setName("Pawan");
		userEntity.setAddress("Hcl Noida");
		userEntity.setEmail("PawanKumar.Pa@hcl.com");
		when(userReposiotry.save(Mockito.any())).thenReturn(userEntity);
		userInfo = mockUserImpl.addUser(userEntity);
		Assert.assertEquals(userInfo.getName(), userEntity.getName());
	}
}
