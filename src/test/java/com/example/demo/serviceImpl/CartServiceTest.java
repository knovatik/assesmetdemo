package com.example.demo.serviceImpl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.aspectj.lang.annotation.Before;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.demo.entity.CartEntity;
import com.example.demo.entity.CartProductEntity;
import com.example.demo.entity.UserEntity;
import com.example.demo.repository.CartProductRepository;
import com.example.demo.repository.CartRepository;
import com.example.demo.repository.UserRepository;
import com.example.demo.service.CartService;


/**
 * test for add Cart Service
 * 
 * @author Pawan Tripathi
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class CartServiceTest {
	@InjectMocks
	private CartService cartservice;
	
	@Mock
	private CartRepository cartRepository;
	@Mock
	private CartProductRepository cartProductrepo;
	@Mock
	private UserRepository userRepository;
	
	
	@Before(value = "")
	public void init() {
		MockitoAnnotations.initMocks(this);
	} 
	/**
	 * test case for user Add to cart
	 */
	@Test
	public void testadddtoCart() {
		UserEntity userEntity = new UserEntity();
		CartEntity cartEntity=new CartEntity();
		cartEntity.setCatrtid(2l);
		cartEntity.setCattotal(2000L);
		
		userEntity.setName("Pawan");
		userEntity.setAddress("Hcl Noida");
		userEntity.setEmail("PawanKumar.Pa@hcl.com");
		cartEntity.setUser(userEntity);
		CartProductEntity cartRequest = new CartProductEntity();
		cartRequest.setProductname("Vovo v 10");
		cartRequest.setPrice(90000.00);
		cartRequest.setId(1L);
		cartRequest.setProductquantity(15.00);

		Optional<UserEntity> userOptional = Optional.of(userEntity);
		
		Optional<CartEntity> cartOptional = Optional.of(cartEntity);;
		
		
	
		when(userRepository.findById(Mockito.any())).thenReturn(userOptional);
		when(cartRepository.findById(Mockito.any())).thenReturn(cartOptional);
	
		
		doAnswer((i) -> {
            return null;
        }).when(cartProductrepo).save(cartRequest);;
		assertEquals(cartservice.addtoCart(cartRequest).getPrice(), cartRequest.getPrice());
	}
	
	
	
	
}
