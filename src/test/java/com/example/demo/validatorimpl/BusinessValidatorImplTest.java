package com.example.demo.validatorimpl;

import org.aspectj.lang.annotation.Before;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.demo.entity.CartProductEntity;
import com.example.demo.entity.ProductEntity;
import com.example.demo.entity.UserEntity;
import com.example.demo.model.CartResponceModel;
import com.example.demo.model.OrderResponceModel;
import com.example.demo.model.ProductResponceModel;
import com.example.demo.model.UserRespnseModel;
import com.example.demo.util.AppConstant;
import com.example.demo.validator.BusinessValidator;

@RunWith(SpringRunner.class)
@SpringBootTest
class BusinessValidatorImplTest {

	@InjectMocks
	private BusinessValidatorImpl businessValidatorImpl;
	@Mock
	private BusinessValidator businessValidator;

	@Before(value = "")
	public void init() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testUserRegistationwithoutEmail() {

		UserEntity userEntity = new UserEntity();
		userEntity.setName("Pawan");
		userEntity.setAddress("Hcl Noida");
		userEntity.setEmail("");
 
		ResponseEntity<Object> responseEntity = businessValidatorImpl.validUserDetails(userEntity);

		UserRespnseModel userRespnseModel = (UserRespnseModel) responseEntity.getBody();
		Assert.assertEquals(userRespnseModel.getMessage(), AppConstant.ENTER_CORRECT_EMAIL);

	}

	@Test
	public void testUserRegistationwithoutName() {

		UserEntity userEntity = new UserEntity();
		userEntity.setName("");
		userEntity.setAddress("Hcl Noida");
		userEntity.setEmail("pawantripathi986@gmail.com");

		ResponseEntity<Object> responseEntity = businessValidatorImpl.validUserDetails(userEntity);

		UserRespnseModel userRespnseModel = (UserRespnseModel) responseEntity.getBody();
		Assert.assertEquals(userRespnseModel.getMessage(), AppConstant.User_NAME_EMPTY);

	}

	@Test
	public void testUserRegistationwithoutAddress() {

		UserEntity userEntity = new UserEntity();
		userEntity.setName("PAwab");
		userEntity.setAddress("");
		userEntity.setEmail("pawantripathi986@gmail.com");

		ResponseEntity<Object> responseEntity = businessValidatorImpl.validUserDetails(userEntity);

		UserRespnseModel userRespnseModel = (UserRespnseModel) responseEntity.getBody();
		Assert.assertEquals(userRespnseModel.getMessage(), AppConstant.User_Address_EMPTY);

	}

	
	@Test
	public void testProductwithoutProductName() {

		ProductEntity prodReq = new ProductEntity();
		prodReq.setProductname("");
		prodReq.setId(1L);
		prodReq.setPrice(90000.00);
		prodReq.setProductquantity(2.00);
		ResponseEntity<Object> responseEntity = businessValidatorImpl.validProductDetail(prodReq);

		ProductResponceModel userRespnseModel = (ProductResponceModel) responseEntity.getBody();
		Assert.assertEquals(userRespnseModel.getMessage(), AppConstant.Product_Name_Emptiy);

	}

	@Test
	public void testProductwithoutProductPrice() {

		ProductEntity prodReq = new ProductEntity();
		prodReq.setProductname("Vovo v 10");
		prodReq.setId(1L);
		prodReq.setPrice(0.0);
		prodReq.setProductquantity(2.00);

		ResponseEntity<Object> responseEntity = businessValidatorImpl.validProductDetail(prodReq);

		ProductResponceModel userRespnseModel = (ProductResponceModel) responseEntity.getBody();
		Assert.assertEquals(userRespnseModel.getMessage(), AppConstant.Product_Price_Emptiy);

	}

	@Test
	public void testProductwithoutProductQuantiy() {

		ProductEntity prodReq = new ProductEntity();
		prodReq.setProductname("Vovo v 10");
		prodReq.setId(1L);
		prodReq.setPrice(90000.00);
		prodReq.setProductquantity(0.0);
		ResponseEntity<Object> responseEntity = businessValidatorImpl.validProductDetail(prodReq);

		ProductResponceModel userRespnseModel = (ProductResponceModel) responseEntity.getBody();
		Assert.assertEquals(userRespnseModel.getMessage(), AppConstant.Product_Quantiy_Emptiy);

	}
	
	
	@Test
	public void testCartwithoutProductName() {

		CartProductEntity prodReq = new CartProductEntity();
		prodReq.setProductname("");
		prodReq.setId(1L);
		prodReq.setPrice(90000.00);
		prodReq.setProductquantity(2.00);
		ResponseEntity<Object> responseEntity = businessValidatorImpl.validCartDetail(prodReq);

		CartResponceModel userRespnseModel = (CartResponceModel) responseEntity.getBody();
		Assert.assertEquals(userRespnseModel.getMessage(), AppConstant.Cart_Name_Emptiy);

	}

	@Test
	public void testCartwithoutProductPrice() {


		CartProductEntity prodReq = new CartProductEntity();
		prodReq.setProductname("Vio v5");
		prodReq.setId(1L);
		prodReq.setPrice(0.0);
		prodReq.setProductquantity(2.00);
		ResponseEntity<Object> responseEntity = businessValidatorImpl.validCartDetail(prodReq);

		CartResponceModel userRespnseModel = (CartResponceModel) responseEntity.getBody();
		Assert.assertEquals(userRespnseModel.getMessage(), AppConstant.Cart_Price_Emptiy);

	}

	@Test
	public void testCartwithoutProductQuantiy() {


		CartProductEntity prodReq = new CartProductEntity();
		prodReq.setProductname("Vio V5");
		prodReq.setId(1L);
		prodReq.setPrice(90000.00);
		prodReq.setProductquantity(0.0);
		ResponseEntity<Object> responseEntity = businessValidatorImpl.validCartDetail(prodReq);
		CartResponceModel userRespnseModel = (CartResponceModel) responseEntity.getBody();
		Assert.assertEquals(userRespnseModel.getMessage(), AppConstant.Cart_Quantiy_Emptiy);

	}

	@Test
	public void testOrderwithoutUserId() {


		
		ResponseEntity<Object> responseEntity = businessValidatorImpl.validUserId("");
		OrderResponceModel userRespnseModel = (OrderResponceModel) responseEntity.getBody();
		Assert.assertEquals(userRespnseModel.getMessage(), AppConstant.User_ID_EMPTY);

	}
	
	@Test
	public void testOrderwithoutOrderId() {
		ResponseEntity<Object> responseEntity = businessValidatorImpl.validOrderId("");
		OrderResponceModel userRespnseModel = (OrderResponceModel) responseEntity.getBody();
		Assert.assertEquals(userRespnseModel.getMessage(), AppConstant.Order_ID_EMPTY);

	}
	
	@Test
	public void testOrderwithoutOrderStatus() {
		ResponseEntity<Object> responseEntity = businessValidatorImpl.validOrderIdandStatus("1", "");
		OrderResponceModel userRespnseModel = (OrderResponceModel) responseEntity.getBody();
		Assert.assertEquals(userRespnseModel.getMessage(), AppConstant.Order_Status_EMPTY);

	}
	
	@Test
	public void testOrderwithoutOrderStatusAndUserID() {
		ResponseEntity<Object> responseEntity = businessValidatorImpl.validOrderIdandStatus("", "INProgres");
		OrderResponceModel userRespnseModel = (OrderResponceModel) responseEntity.getBody();
		Assert.assertEquals(userRespnseModel.getMessage(), AppConstant.Order_ID_EMPTY);

	}
	
	@Test
	public void testOrderwithoutCartID() {
		ResponseEntity<Object> responseEntity = businessValidatorImpl.validCartId("");
		OrderResponceModel userRespnseModel = (OrderResponceModel) responseEntity.getBody();
		Assert.assertEquals(userRespnseModel.getMessage(), AppConstant.Cart_id_Emptiy);

	}
	
	

	
	 @Test
	public void validOrderIdwitoutProduct() {
		ResponseEntity<Object> responseEntity = businessValidatorImpl.validOrderIdProductidUseridandquanity("","1","1","2");
		OrderResponceModel userRespnseModel = (OrderResponceModel) responseEntity.getBody();
		Assert.assertEquals(userRespnseModel.getMessage(), AppConstant.Order_ID_EMPTY);

	}
	 @Test
		public void validOrderIdwitoutProdutID() {
			ResponseEntity<Object> responseEntity = businessValidatorImpl.validOrderIdProductidUseridandquanity("1","","1","2");
			OrderResponceModel userRespnseModel = (OrderResponceModel) responseEntity.getBody();
			Assert.assertEquals(userRespnseModel.getMessage(), AppConstant.Product_ID_Emptiy);

		}
	 @Test
		public void validOrderIdwitoutUserID() {
			ResponseEntity<Object> responseEntity = businessValidatorImpl.validOrderIdProductidUseridandquanity("1","1","","2");
			OrderResponceModel userRespnseModel = (OrderResponceModel) responseEntity.getBody();
			Assert.assertEquals(userRespnseModel.getMessage(), AppConstant.User_ID_EMPTY);

		}
	 
	 @Test
		public void validOrderIdwitoutQuantity() {
			ResponseEntity<Object> responseEntity = businessValidatorImpl.validOrderIdProductidUseridandquanity("1","1","1","");
			OrderResponceModel userRespnseModel = (OrderResponceModel) responseEntity.getBody();
			Assert.assertEquals(userRespnseModel.getMessage(), AppConstant.Product_Quantiy_Emptiy);

		}
}
