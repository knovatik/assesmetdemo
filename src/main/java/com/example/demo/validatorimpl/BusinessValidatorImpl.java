package com.example.demo.validatorimpl;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.example.demo.entity.CartProductEntity;
import com.example.demo.entity.ProductEntity;
import com.example.demo.entity.UserEntity;
import com.example.demo.model.CartResponceModel;
import com.example.demo.model.OrderResponceModel;
import com.example.demo.model.ProductResponceModel;
import com.example.demo.model.UserRespnseModel;
import com.example.demo.util.AppConstant;
import com.example.demo.util.StringUtils;
import com.example.demo.validator.BusinessValidator;

public class BusinessValidatorImpl implements BusinessValidator{
	@Override
	public ResponseEntity<Object> validUserDetails(UserEntity userModel) {
		if (!StringUtils.isEmailPattern(userModel.getEmail())) {
			return new ResponseEntity<Object>(new UserRespnseModel(true, null,AppConstant.ENTER_CORRECT_EMAIL),
					HttpStatus.OK);
		}  else if (StringUtils.isNullOrEmpty(userModel.getName())) {
			return new ResponseEntity<Object>(new UserRespnseModel(true,null, AppConstant.User_NAME_EMPTY), 
					HttpStatus.OK);
		}  else if (StringUtils.isNullOrEmpty(userModel.getAddress())) {
			return new ResponseEntity<Object>(new UserRespnseModel(true, null,AppConstant.User_Address_EMPTY), 
					HttpStatus.OK);
		}else {
			return null;
		}
	}

	@Override
	public ResponseEntity<Object> validProductDetail(ProductEntity productEntity) {
		if (StringUtils.isNullOrEmpty(productEntity.getProductname())) {
			return new ResponseEntity<Object>(new ProductResponceModel(true,null, AppConstant.Product_Name_Emptiy), 
					HttpStatus.OK);
		}  else if (productEntity.getPrice()==0.0) {
			return new ResponseEntity<Object>(new ProductResponceModel(true, null,AppConstant.Product_Price_Emptiy), 
					HttpStatus.OK);
		}else if (productEntity.getProductquantity()==0.0) {
			return new ResponseEntity<Object>(new ProductResponceModel(true, null,AppConstant.Product_Quantiy_Emptiy), 
					HttpStatus.OK);
		}else {
			return null;
		}
	}

	@Override
	public ResponseEntity<Object> validCartDetail(CartProductEntity cartProductEntity) {
		if (StringUtils.isNullOrEmpty(cartProductEntity.getProductname())) {
			return new ResponseEntity<Object>(new CartResponceModel(true,null, AppConstant.Cart_Name_Emptiy), 
					HttpStatus.OK);
		}  else if (cartProductEntity.getPrice()==0.0) {
			return new ResponseEntity<Object>(new CartResponceModel(true, null,AppConstant.Cart_Price_Emptiy), 
					HttpStatus.OK);
		}else if (cartProductEntity.getProductquantity()==0.0) {
			return new ResponseEntity<Object>(new CartResponceModel(true, null,AppConstant.Cart_Quantiy_Emptiy), 
					HttpStatus.OK);
		}else {
			return null;
		}
	}

	
	@Override
	public ResponseEntity<Object> validUserId(String useid) {
		 if (StringUtils.isNullOrEmpty(useid)) {
			return new ResponseEntity<Object>(new OrderResponceModel(true, null,AppConstant.User_ID_EMPTY), 
					HttpStatus.OK);
		}else {
			
		}	return null;
		
	}

	@Override
	public ResponseEntity<Object> validOrderId(String orderid) {
		 if (StringUtils.isNullOrEmpty(orderid)) {
				return new ResponseEntity<Object>(new OrderResponceModel(true, null,AppConstant.Order_ID_EMPTY), 
						HttpStatus.OK);
			}else {
				
			}	return null;
	}

	@Override
	public ResponseEntity<Object> validOrderIdandStatus(String oderid, String status) {
	if (StringUtils.isNullOrEmpty(oderid)) {
		return new ResponseEntity<Object>(new OrderResponceModel(true, null,AppConstant.Order_ID_EMPTY), 
				HttpStatus.OK);
	}else if (StringUtils.isNullOrEmpty(status)) {
		return new ResponseEntity<Object>(new OrderResponceModel(true, null,AppConstant.Order_Status_EMPTY), 
				HttpStatus.OK);
	}else {
		return null;
	}
	}

	@Override
	public ResponseEntity<Object> validCartId(String cartid) {
		 if (StringUtils.isNullOrEmpty(cartid)) {
				return new ResponseEntity<Object>(new OrderResponceModel(true, null,AppConstant.Cart_id_Emptiy), 
						HttpStatus.OK);
			}else {
				
			}	return null;
			
	}

	@Override
	public ResponseEntity<Object> validOrderIdProductidUseridandquanity(String orderid, String productid, String userid,
			String quantity) {
		if (StringUtils.isNullOrEmpty(orderid)) {
			return new ResponseEntity<Object>(new OrderResponceModel(true, null,AppConstant.Order_ID_EMPTY), 
					HttpStatus.OK);
		}else if (StringUtils.isNullOrEmpty(productid)) {
			return new ResponseEntity<Object>(new OrderResponceModel(true, null,AppConstant.Product_ID_Emptiy), 
					HttpStatus.OK);
		}else if (StringUtils.isNullOrEmpty(userid)) {
			return new ResponseEntity<Object>(new OrderResponceModel(true, null,AppConstant.User_ID_EMPTY), 
					HttpStatus.OK);
		}else if (StringUtils.isNullOrEmpty(quantity)) {
			return new ResponseEntity<Object>(new OrderResponceModel(true, null,AppConstant.Product_Quantiy_Emptiy), 
					HttpStatus.OK);
		}else {
			return null;
		}
		}
	




}
