package com.example.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.entity.CartEntity;
/**
 * @author Pawan
 *
 */
@Repository
public interface CartRepository extends JpaRepository<CartEntity, Long> {
	
//	@Query("DELETE from cart_entity where user_user_id=1")
//	public int deleteById(long id);

}

