package com.example.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.example.demo.entity.CartProductEntity;
@Repository
public interface CartProductRepository  extends JpaRepository<CartProductEntity, Long> {
	
	
	 @Modifying
	 @Query(value = "SELECT * FROM CART_PRODUCT_ENTITY  WHERE cart_id = ?id", nativeQuery = true)
	 public int  finduserProductBbyID(@Param("id") long id);

}
