package com.example.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.example.demo.entity.OrderDataEntity;
@Repository
public interface OrderDataRepository extends JpaRepository<OrderDataEntity, Long> {
	
	 @Query(value="SELECT * FROM ORDER_DATA_ENTITY where ORDERNEW_ID=:order_id", nativeQuery = true) 
	 public List<OrderDataEntity> findOrderByID_Table(@Param("order_id")String id);
	 
}
