package com.example.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.entity.OrderEntity;
/**
 * @author Pawan Tripathi
 *
 */
@Repository
public interface OrderRepository extends JpaRepository<OrderEntity, Long> {
	
	
	

}

