package com.example.demo;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.example.demo.validator.BusinessValidator;
import com.example.demo.validatorimpl.BusinessValidatorImpl;



/**Create AppConfig class
 * Create Bean
 * @author Pawan Kumar
 *
 */
@Configuration
public class AppConfig {
	@Bean
	BusinessValidator findIValidator() {
		return new BusinessValidatorImpl();
	}
}
