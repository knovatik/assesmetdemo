package com.example.demo.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class UserEntity {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "user_id")
	public Long id;
	@OneToOne(mappedBy ="user" )
	public CartEntity cartEntity;
	
	public CartEntity getCartEntity() {
		return cartEntity;
	}

	public UserEntity(Long id, CartEntity cartEntity, String name, String address, String email) {
		super();
		this.id = id;
		this.cartEntity = cartEntity;
		this.name = name;
		this.address = address;
		this.email = email;
	}

	public void setCartEntity(CartEntity cartEntity) {
		this.cartEntity = cartEntity;
	}

	public UserEntity() {
		super();
		// TODO Auto-generated constructor stub
	}

	

	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}
	@Column
	private String name;
	@Column
	private String address;
	@Column
	private String email;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
}
