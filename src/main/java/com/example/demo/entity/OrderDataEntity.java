package com.example.demo.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

/**
 * Create Order table
 * @author Pawan KU
 *
 */
@Entity
public class OrderDataEntity {
	
	@Id
	@Column(name = "product_id")
	private Long id;
	@Column
	private Double productquantity;
	
	@ManyToOne( cascade =  CascadeType.ALL)
	@JoinColumn(name = "order_id", insertable = true, updatable = false)
	private OrderEntity orderEntity;

	
	public OrderEntity getOrderEntity() {
		return orderEntity;
	}

	public void setOrderEntity(OrderEntity orderEntity) {
		this.orderEntity = orderEntity;
	}

	@OneToOne
	@JoinColumn(name = "user_id" )
	private UserEntity user;
	public UserEntity getUser() {
		return user;
	}

	public void setUser(UserEntity user) {
		this.user = user;
	}

	public OrderDataEntity() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Column
	private Double price;
	@Column
	private String productname;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Double getProductquantity() {
		return productquantity;
	}

	public void setProductquantity(Double productquantity) {
		this.productquantity = productquantity;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public String getProductname() {
		return productname;
	}

	public void setProductname(String productname) {
		this.productname = productname;
	}
}