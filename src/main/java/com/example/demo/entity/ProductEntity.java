package com.example.demo.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
/**
 * Create Product table
 * @author PawanKU
 *
 */
@Entity
public class ProductEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "product_id")
	private Long id;
	@Column
	private Double productquantity;
	public ProductEntity() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Column
	private Double price;
	@Column
	private String productname;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Double getProductquantity() {
		return productquantity;
	}

	public void setProductquantity(Double productquantity) {
		this.productquantity = productquantity;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public String getProductname() {
		return productname;
	}

	public void setProductname(String productname) {
		this.productname = productname;
	}
}
