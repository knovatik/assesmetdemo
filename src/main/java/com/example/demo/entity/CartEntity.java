package com.example.demo.entity;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
/**
 * Create Cart table
 * @author PawanKU
 *
 */

@Entity
@Table(name="cart_entity")
public class CartEntity {
	public CartEntity(Set<CartProductEntity> items, Long catrtid, Long cattotal, UserEntity user) {
		super();
		this.items = items;
		this.catrtid = catrtid;
		this.cattotal = cattotal;
		this.user = user;
	}


	public Long getCatrtid() {
		return catrtid;
	}
	
	
	public void setCatrtid(Long catrtid) {
		this.catrtid = catrtid;
	}
	
	public Long getCattotal() {
		return cattotal;
	}
	
	public CartEntity() {
		super();
		// TODO Auto-generated constructor stub
	}
	public void setCattotal(Long cattotal) {
		this.cattotal = cattotal;
	}
	public UserEntity getUser() {
		return user;
	}
	public void setUser(UserEntity user) {
		this.user = user;
	}
	
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "cart")
    private Set<CartProductEntity> items;	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "cart_id")
	private Long catrtid;
	@Column
	private Long cattotal;

	@OneToOne
	@JoinColumn(name="user_id")
	
	private UserEntity user;

	public Set<CartProductEntity> getItems() {
		return items;
	}


	public void setItems(Set<CartProductEntity> items) {
		this.items = items;
	}
	
	

}
