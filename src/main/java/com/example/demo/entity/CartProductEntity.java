package com.example.demo.entity;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

/**
 * Create Cart table
 * @author Pawan Kumar
 *
 */
@Entity
public class CartProductEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "product_id")
	private Long id;
	@Column
	private Double productquantity;
	
	@ManyToOne( cascade =  CascadeType.ALL)
	@JoinColumn(name = "cart_id" )
	private CartEntity cart;

	public CartEntity getCart() {
		return cart;
	}

	public void setCart(CartEntity cart) {
		this.cart = cart;
	}

	@OneToOne
	@JoinColumn(name = "user_id" )
	private UserEntity user;
	public UserEntity getUser() {
		return user;
	}

	public void setUser(UserEntity user) {
		this.user = user;
	}

	public CartProductEntity() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Column
	private Double price;
	@Column
	private String productname;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Double getProductquantity() {
		return productquantity;
	}

	public void setProductquantity(Double productquantity) {
		this.productquantity = productquantity;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public String getProductname() {
		return productname;
	}

	public void setProductname(String productname) {
		this.productname = productname;
	}
}
