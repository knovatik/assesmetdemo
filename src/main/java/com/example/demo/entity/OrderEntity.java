package com.example.demo.entity;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;


/**
 * Create Order table
 * @author PawanKU
 *
 */
@Entity
public class OrderEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "order_id")

	private Long id;
	@Column
	private  String ordersatus;

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}

	public String getOrdersatus() {
		return ordersatus;
	}
	
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "orderEntity")
    private Set<OrderDataEntity> items;	
	
	public Set<OrderDataEntity> getItems() {
		return items;
	}
	public void setItems(Set<OrderDataEntity> items) {
		this.items = items;
	}
	public OrderEntity(String ordersatus, UserEntity userEntity) {
		super();
		
		this.ordersatus = ordersatus;
		this.user = userEntity;
		
	}

	public OrderEntity() {
		super();
		// TODO Auto-generated constructor stub
	}

	public void setOrdersatus(String ordersatus) {
		this.ordersatus = ordersatus;
	}

	public UserEntity getUser() {
		return user;
	}

	public void setUser(UserEntity user) {
		this.user = user;
	}
	
	@OneToOne( cascade =  CascadeType.ALL)
	private UserEntity user;
	
}
