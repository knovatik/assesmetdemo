package com.example.demo.service;

import com.example.demo.entity.UserEntity;


/**
 * Method Use to register user 
 * @Param UserEntity
 * @Table UserEntity  is use to map user data 
 * @Action Register user 
 * @Returntype  UserEntity
 */
public interface UserService {
	public UserEntity addUser(UserEntity user);
	
}
