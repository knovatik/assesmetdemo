package com.example.demo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entity.ProductEntity;
import com.example.demo.repository.ProductRepository;

@Service
public class ProductService {
	@Autowired
	ProductRepository productrepo;
	/**
	 * Method Use to addProduct
	 * @Param ProductEntity
	 * @Table ProductEntity  is use to map product data 
	 * @Action Save Product 
	 * @Returntype  ProductEntity
	 */
	public ProductEntity  addProduct(ProductEntity productentity) {
		
		productrepo.save(productentity);
		
		return productentity;
		 
	}
	
	

}
