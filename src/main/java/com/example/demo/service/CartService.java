package com.example.demo.service;

import java.util.Optional;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entity.CartEntity;
import com.example.demo.entity.CartProductEntity;
import com.example.demo.entity.UserEntity;
import com.example.demo.repository.CartProductRepository;
import com.example.demo.repository.CartRepository;
import com.example.demo.repository.UserRepository;

@Service
public class CartService {
	
	
	@Autowired
	CartProductRepository productrepo;
	
	@Autowired
	UserRepository user_repo;
	
	@Autowired
	CartRepository cart_repo;
	
	/**
	 * Method Use to addtoCart 
	 * @Param CartProductEntity 
	 * @Table CartProductEntity  is use to map cart  data using one to many mapping in which we have three columns order status ,ordered and user id
	 * @Table CartEntity save data to cart
	 * @Action add  to cart 
	 * @Return type  CartProductEntity
	 */	
	@Transactional
	public CartProductEntity addtoCart(@Valid CartProductEntity productentity) {
		Optional<UserEntity> optional=user_repo.findById(productentity.getId());
		Optional<CartEntity> cartOptional=cart_repo.findById(productentity.getId());
	    CartEntity cart=null; try { cart=cartOptional.get(); } catch(Exception e) { }
		 if(cart!=null) { double quantity=cart.getCattotal();
		  quantity=quantity+(Double.valueOf((long)
		  (productentity.getPrice()*productentity.getProductquantity())));
		  cart.setCattotal(Long.valueOf((long) ((quantity))));
		  cart.setUser(optional.get()); } else { cart=new CartEntity();
		  cart.setCattotal(Long.valueOf((long)
		  (productentity.getPrice()*productentity.getProductquantity())));
		  cart.setUser(optional.get()); }
		productentity.setUser(optional.get());
		 productentity.setCart(cart); 
		productrepo.save(productentity);
		return productentity;
	}
	
	

}
