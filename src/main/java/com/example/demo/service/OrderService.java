package com.example.demo.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import com.example.demo.entity.CartEntity;
import com.example.demo.entity.CartProductEntity;
import com.example.demo.entity.OrderDataEntity;
import com.example.demo.entity.OrderEntity;
import com.example.demo.entity.UserEntity;
import com.example.demo.repository.CartProductRepository;
import com.example.demo.repository.CartRepository;
import com.example.demo.repository.OrderDataRepository;
import com.example.demo.repository.OrderRepository;
import com.example.demo.repository.UserRepository;

@Service
public class OrderService {
	@Autowired
	OrderRepository order_repo;
	@Autowired
	UserRepository user_repo;
	@Autowired
	CartRepository cart_repo;
	@Autowired
	CartProductRepository cart_data_repo;
	@Autowired
	OrderDataRepository orderdatarepo;
	/**
	 * Method Use to Submit Order and after submitting order it will delete all
	 * records of perticular cart
	 * 
	 * @Param Long Cart id
	 * @Table OrderEntity is use to map order data using one to many mapping in
	 *        which we have three columns order status ,ordered and user id
	 * @Table OrderDataEntity is used to save order to database wish is mapped to
	 *        order entity in which we have three columns ordered,product id,product
	 *        price,product quantity and user id
	 * @Table CartProductEntity is used get all data from cart
	 * @Action Get all product data from cart , place order and delete all data from
	 *         cart
	 * @Return type OrderDataEntity
	 */

	@Autowired
	private JdbcTemplate jdbcTemplate;

	public List<OrderDataEntity> submitOrder(Long cartid) {
		
		Optional<CartEntity> cart = cart_repo.findById(cartid);
		
		Optional<UserEntity> user = user_repo.findById(cart.get().getUser().getId());
		OrderEntity orderentity = new OrderEntity();
		orderentity.setOrdersatus("InProgress");
		orderentity.setUser(user.get());
		List<CartProductEntity> cartProductEntities = cart_data_repo.findAll();
		List<OrderDataEntity> orderdatalIst = new ArrayList<>();
		for (CartProductEntity cartProductEntity : cartProductEntities) {
			OrderDataEntity orderDataEntity = new OrderDataEntity();
			orderDataEntity.setId(cartProductEntity.getCart().getCatrtid());
			orderDataEntity.setPrice(cartProductEntity.getPrice());
			orderDataEntity.setProductname(cartProductEntity.getProductname());
			orderDataEntity.setProductquantity(cartProductEntity.getProductquantity());
			orderDataEntity.setUser(user.get());
			orderDataEntity.setOrderEntity(orderentity);

			orderdatalIst.add(orderDataEntity);

		}
		List<OrderDataEntity> cartProductEntityListNew = orderdatarepo.saveAll(orderdatalIst);
		cart_repo.deleteById(cart.get().getCatrtid());

		return cartProductEntityListNew;
	}

	/**
	 * Method Use to Short data from Order table on behalf of short type
	 * 
	 * @Param String sort_type
	 * @Table OrderEntity is use to map order data using one to many mapping in
	 *        which we have three columns order status ,ordered and user id
	 * @Action Get all product data on behalf of shorting type
	 * @Return type OrderEntity
	 */
	public List<OrderDataEntity> sortType(String sort_type) {
		List<OrderDataEntity> order_ent = null;
		if (sort_type.equalsIgnoreCase("productname")) {
			order_ent = orderdatarepo.findAll(Sort.by(Sort.Direction.ASC, "productname"));
		} else if(sort_type.equalsIgnoreCase("price")){
			order_ent = orderdatarepo.findAll(Sort.by(Sort.Direction.DESC, "price"));
		}else {
			order_ent = orderdatarepo.findAll(Sort.by(Sort.Direction.DESC, "id"));
		}
		return order_ent;
	}
	
	

	/**
	 * Method Use to Short data from Order table on behalf of short type
	 * 
	 * @Param String sort_type
	 * @Table OrderEntity is use to map order data using one to many mapping in
	 *        which we have three columns order status ,ordered and user id
	 * @Table OrderDataEntity is used to save order to database wish is mapped to
	 *        order entity in which we have three columns ordered,product id,product
	 *        price,product quantity and user id
	 * @Action Get all Order data on behalf of Order id
	 * @Returntype List of OrderDataEntity
	 */
	public List<OrderDataEntity> getSingleOrder(String order_id) {

		String sql = "SELECT * FROM ORDER_DATA_ENTITY where ORDER_ID=?";
		List<Map<String, Object>> orderDataEntity = new ArrayList<>();
		List<OrderDataEntity> orders = new ArrayList<>();
		orderDataEntity = jdbcTemplate.queryForList(sql, new Object[] { order_id });
		for (Map row : orderDataEntity) {
			OrderDataEntity obj = new OrderDataEntity();
			obj.setId(((Long) row.get("PRODUCT_ID")).longValue());
			obj.setProductname((String) row.get("PRODUCTNAME"));
			obj.setPrice((Double) row.get("PRICE"));
			obj.setProductquantity((Double) row.get("PRODUCTQUANTITY"));
			orders.add(obj);
		}

		return orders;
	}

	/**
	 * Method Use to Update Shipping Address
	 * 
	 * @Param String sort_type
	 * @Table OrderEntity is use to map order data using one to many mapping in
	 *        which we have three columns order status ,ordered and user id
	 * @Action Update Address data on behalf of Order id
	 * @Returntype void
	 */
	public void updateAddress(String order_id, String address) {
		Optional<OrderEntity> entity = order_repo.findById(Long.valueOf(order_id));

		if (entity.get().getOrdersatus().equals("InProgress")) {

			UserEntity user = entity.get().getUser();

			user.setAddress(address);

			UserEntity updatedEntity = user_repo.save(user);

		} else {

			throw new RuntimeException("Error in Updating Address");
		}

	}

	/**
	 * Method Use to Cancel Order when Order is InProgress
	 * 
	 * @Param String order_id,String status
	 * @Table OrderEntity is use to map order data using one to many mapping in
	 *        which we have three columns order status ,ordered and user id
	 * @Action Get all Order data on behalf of Order id
	 * @Returntype void
	 */
	public void cancelOrder(String order_id, String status) {
		Optional<OrderEntity> entity = order_repo.findById(Long.valueOf(order_id));
		if (entity.get().getOrdersatus().equalsIgnoreCase("InProgress")) {
			OrderEntity orderentiy = entity.get();
			orderentiy.setOrdersatus("Canceled");
			order_repo.save(orderentiy);
		} else {
			throw new RuntimeException("Error in canceling Order");
		}
	}

	/**
	 * Method Use to Update Order Quantity when Order is InProgress
	 * 
	 * @Param String order_id,String quantity,String userid,String productid
	 * @Table OrderEntity is use to map order data using one to many mapping in
	 *        which we have three columns order status ,ordered and user id
	 * @Action Get all Order data on behalf of Order id
	 * @Returntype void
	 */
	public void updateOrderQuanity(String order_id, String productid, String quantity, String userid) {
		String sql1 = "SELECT * FROM ORDER_DATA_ENTITY where ORDER_ID=? and PRODUCT_ID=" + productid;
		List<Map<String, Object>> orderDataEntity = new ArrayList<>();
		List<OrderDataEntity> orders = new ArrayList<>();
		Optional<OrderEntity> entity = order_repo.findById(Long.valueOf(order_id));

		if (entity.get().getOrdersatus().equals("InProgress")) {

			Double price = null;
			Optional<UserEntity> user = user_repo.findById(Long.valueOf(userid));
			Optional<OrderEntity> orderentiy = order_repo.findById(Long.valueOf(order_id));
			orderDataEntity = jdbcTemplate.queryForList(sql1, new Object[] { order_id });
			for (Map row : orderDataEntity) {
				OrderDataEntity obj = new OrderDataEntity();
				if ((Long) row.get("PRODUCT_ID") == Long.valueOf(productid)) {
					price = (Double) row.get("PRICE");
				}
				obj.setId(((Long) row.get("PRODUCT_ID")).longValue());
				obj.setProductname((String) row.get("PRODUCTNAME"));
				obj.setPrice((Double) row.get("PRICE"));
				obj.setProductquantity((Double) row.get("PRODUCTQUANTITY"));
				obj.setUser(user.get());
				obj.setOrderEntity(orderentiy.get());
				orders.add(obj);
			}

			String sql = "Update  ORDER_DATA_ENTITY set PRODUCTQUANTITY=" + quantity + " , " + " PRICE= "
					+ (Double.valueOf(quantity) * price) + "  where  ORDER_ID=" + order_id + " and " + "PRODUCT_ID="
					+ productid;

			jdbcTemplate.update(sql);

		} else {
			throw new RuntimeException("Error in canceling Order");
		}

	}

	/**
	 * Method Use to Update Order Status
	 * 
	 * @Param String order_id,String status
	 * @Table OrderEntity is use to map order data using one to many mapping in
	 *        which we have three columns order status ,ordered and user id
	 * @Action Get all Order data on behalf of Order id
	 * @Returntype void
	 */
	public void updateOrderStatus(String id, String status) {
		// Optional<UserEntity> user_entity=user_repo.findById(Long.valueOf(user_id));
		Optional<OrderEntity> entity = order_repo.findById(Long.valueOf(id));
		OrderEntity orderentiy = entity.get();
		orderentiy.setOrdersatus(status);
		order_repo.save(orderentiy);

	}

}
