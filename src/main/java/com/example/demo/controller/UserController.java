package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.example.demo.entity.UserEntity;
import com.example.demo.impl.UserImpl;
import com.example.demo.util.AppConstant;
import com.example.demo.util.NoRespnseModel;
import com.example.demo.validator.BusinessValidator;
/**
 * Action related User
 * @author Pawan Tripathi
 *
 */
@RestController
public class UserController {
	@Autowired
	private UserImpl iUser;
	
	@Autowired(required = true)
	BusinessValidator businessValidator;
	
	
	/**
	 * Post mathod for User Registration
	 * @param  User Request
	 * @return ResponseEntity<Object>
	 * @Excption User Not Found
	 */
	@PostMapping("/registeruser")
	public ResponseEntity<Object> userRegistration(@RequestBody UserEntity request) {
		ResponseEntity<Object> responseEntity = businessValidator.validUserDetails(request);
		if (responseEntity == null) {
			try {
				UserEntity userData = (UserEntity) iUser.addUser(request);
				return new ResponseEntity<Object>((new NoRespnseModel(true, userData,AppConstant.USER_REGISTRATION_SUCCESSFULLY)), HttpStatus.CREATED);
		
		} catch (Exception e) {
			return new ResponseEntity<Object>(new NoRespnseModel(false, null,e.getMessage()),
					HttpStatus.INTERNAL_SERVER_ERROR);
		}
	} else {
		return responseEntity;
	}
	}
}
