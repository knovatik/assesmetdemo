 package com.example.demo.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.entity.CartProductEntity;
import com.example.demo.model.CartResponceModel;
import com.example.demo.service.CartService;
import com.example.demo.util.AppConstant;
import com.example.demo.util.NoRespnseModel;
import com.example.demo.validator.BusinessValidator;

/**
 * @author Pawan Tripathi
 *
 */
@RestController
@RequestMapping
public class CartController {
	@Autowired
	CartService cartService;
	@Autowired(required = true)
	BusinessValidator businessValidator;
	/**
	 * Post  add to Cart Order on behalf of CartProductEntity
	 * @RequestBody   CartProductEntity 
	 * @return ResponseEntity<Object>
	 * @Excption 404  Not Found, 500 Internal Server Error
	 */

	@PostMapping("/addtocart")
	public ResponseEntity<Object> addtoCart(@Valid @RequestBody CartProductEntity cartentity) {
		
		ResponseEntity<Object> responseEntity = businessValidator.validCartDetail(cartentity);
		if (responseEntity == null) {
			try {
				CartProductEntity catEntity=cartService.addtoCart(cartentity);
				return new ResponseEntity<Object>((new CartResponceModel(true, catEntity,AppConstant.Product_Cart_Responce)), HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<Object>(new NoRespnseModel(false, null,e.getMessage()),
					HttpStatus.INTERNAL_SERVER_ERROR);
		}}else {
			return responseEntity;
		}}
		
}
