package com.example.demo.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.entity.ProductEntity;
import com.example.demo.model.ProductResponceModel;
import com.example.demo.service.ProductService;
import com.example.demo.util.AppConstant;
import com.example.demo.util.NoRespnseModel;
import com.example.demo.validator.BusinessValidator;

@RestController
@RequestMapping

/**
 * Action related Add Product
 * @author Pawan Tripathi
 *
 */


public class ProductController {
	@Autowired
	ProductService productservice;
	
	@Autowired(required = true)
	BusinessValidator businessValidator;
	/**
	 * Post Method for Add Product
	 * @param  ProductEntity Request
	 * @return ResponseEntity<Object>
	 * @Excption Internal Server Error
	 */
	@PostMapping("/addproduct")
	public ResponseEntity<Object> addProduct(@Valid @RequestBody ProductEntity product) {
		
		ResponseEntity<Object> responseEntity = businessValidator.validProductDetail(product);
		if (responseEntity == null) {
			try {
				productservice.addProduct(product);
				return new ResponseEntity<Object>((new ProductResponceModel(true, product,AppConstant.Product_Responce)), HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<Object>(new NoRespnseModel(false, null,e.getMessage()),
					HttpStatus.INTERNAL_SERVER_ERROR);
		}}else {
			return responseEntity;
		}}
		}

