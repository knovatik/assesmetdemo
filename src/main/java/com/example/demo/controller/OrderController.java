package com.example.demo.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.entity.ResponseModel;
import com.example.demo.model.OrderDataResponceModel;
import com.example.demo.model.OrderResponceModel;
import com.example.demo.service.OrderService;
import com.example.demo.util.AppConstant;
import com.example.demo.util.NoRespnseModel;
import com.example.demo.validator.BusinessValidator;

/**
 * @author Pawan Tripathi
 *
 */

@RestController
public class OrderController {
	@Autowired
	OrderService orderservice;
	@Autowired(required = true)
	BusinessValidator businessValidator;

	/**
	 * Get mathod for Submit Order
	 * @PathVariable user_id
	 * @return ResponseEntity<Object>
	 * @Excption 404 Not Found,Internal Server Error
	 */

	@GetMapping("/submitOrder/{cart_id}")
	public ResponseEntity<Object> submitOrder(@Valid @PathVariable("cart_id") String cart_id) {
		ResponseEntity<Object> responseEntity = businessValidator.validUserId(cart_id);
		if (responseEntity == null) {
			try {
				orderservice.submitOrder(Long.valueOf(cart_id));
				return new ResponseEntity<Object>((new OrderResponceModel(true, null, AppConstant.ORDER_SUCCES_STRING)),
						HttpStatus.CREATED);
			} catch (Exception e) {
				return new ResponseEntity<Object>(new NoRespnseModel(false, null, e.getMessage()),
						HttpStatus.INTERNAL_SERVER_ERROR);
			}
		} else {
			return responseEntity;

		}

	}

	/**
	 * Get getAllOrder on behalf of short type
	 * 
	 * @PathVariable sort_type
	 * @return ResponseEntity<Object>
	 * @Excption 404 Not Found,Internal Server Error
	 */
	@GetMapping("/getshorteddata/{sort_type}")
	public ResponseEntity<Object> shortOrder(@PathVariable("sort_type") String sort_type) {
		try {

			return new ResponseEntity<Object>(
					new ResponseModel(true, "Successfully", orderservice.sortType(sort_type), 1), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<Object>(new ResponseModel(false, e.getMessage(), null, 0),
					HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	/**
	 * Get getSingle Order on behalf of id
	 * 
	 * @PathVariable Order id
	 * @return ResponseEntity<Obje ct>
	 * @Excption 404 Not Found,Internal Server Error
	 */
	
	@GetMapping("/getOrder/{order_id}")
	public ResponseEntity<Object> getOrder(@PathVariable("order_id") String order_id) {
		ResponseEntity<Object> responseEntity = businessValidator.validOrderId(order_id);
		if (responseEntity == null) {
			try {
				return new ResponseEntity<Object>((new OrderDataResponceModel(true,
						orderservice.getSingleOrder(order_id), "no data")), HttpStatus.CREATED);
			} catch (Exception e) {
				return new ResponseEntity<Object>(new NoRespnseModel(false, null, e.getMessage()),
						HttpStatus.INTERNAL_SERVER_ERROR);
			}
		} else {
			return responseEntity;
		}
	}

	/**
	 * Post updateOrderQuantity Order on behalf of order_id,address
	 * 
	 * @RequestParam Order id
	 * @return ResponseEntity<Object>
	 * @Excption 404 Not Found, 500 Internal Server Error
	 */
	@PostMapping("/updateaddress")
	public ResponseEntity<Object> updateAddress(@RequestParam("order_id") String id,
			@RequestParam("address") String address) {

		ResponseEntity<Object> responseEntity = businessValidator.validOrderId(id);
		if (responseEntity == null) {
			try {

				orderservice.updateAddress(id, address);
				return new ResponseEntity<Object>(new ResponseModel(true, "Address Updated Successfully", null, 1),
						HttpStatus.OK);
			} catch (Exception e) {
				return new ResponseEntity<Object>(new NoRespnseModel(false, null, e.getMessage()),
						HttpStatus.INTERNAL_SERVER_ERROR);
			}
		} else {
			return responseEntity;

		}
	}

	/**
	 * Post updateOrderQuantity Order on behalf of order_id,status
	 * 
	 * @RequestParam Order id
	 * @return ResponseEntity<Object>
	 * @Excption 404 Not Found, 500 Internal Server Error
	 */

	@PostMapping("/updateOrderstatus")
	public ResponseEntity<Object> updateOrderStatus(@RequestParam("order_id") String id,
			@RequestParam("status") String status) {

		ResponseEntity<Object> responseEntity = businessValidator.validOrderIdandStatus(id, status);
		if (responseEntity == null) {
			try {
				orderservice.updateOrderStatus(id, status);

				return new ResponseEntity<Object>(
						new ResponseModel(true, AppConstant.ORDER_STATUS_STRING, null, 1), HttpStatus.OK);

			} catch (Exception e) {

				return new ResponseEntity<Object>(new ResponseModel(false, e.getMessage(), null, 0),
						HttpStatus.INTERNAL_SERVER_ERROR);
			}
		} else {
			return responseEntity;

		}
	}

	/**
	 * Post updateOrderQuantity Order on behalf of order_id,product id,quantity,user
	 * id
	 * 
	 * @RequestParam Order id
	 * @return ResponseEntity<Object>
	 * @Excption 404 Not Found, 500 Internal Server Error
	 */

	@PostMapping("/updateQuantity")
	public ResponseEntity<Object> updateOrderQuantity(@RequestParam("order_id") String id,
			@RequestParam("prouct_id") String productid, @RequestParam("quanitity") String quantity,
			@RequestParam("user_id") String userid) {

		ResponseEntity<Object> responseEntity = businessValidator.validOrderIdProductidUseridandquanity(id, productid,
				userid, quantity);
		if (responseEntity == null) {
			try {

				orderservice.updateOrderQuanity(id, productid, quantity, userid);
				return new ResponseEntity<Object>(
						new ResponseModel(true, AppConstant.Quantity_Updated_Succesfully, null, 1), HttpStatus.OK);
			} catch (Exception e) {

				return new ResponseEntity<Object>(new ResponseModel(false, e.getMessage(), null, 0),
						HttpStatus.INTERNAL_SERVER_ERROR);
			}
		} else {
			return responseEntity;

		}

	}

	/**
	 * Post cancelOrder Order on behalf of order_id
	 * 
	 * @RequestParam Order id
	 * @return ResponseEntity<Object>
	 * @Excption 404 Not Found, 500 Internal Server Error
	 */

	@PostMapping("/cancelOrder")
	public ResponseEntity<Object> cancel(@RequestParam("order_id") String id, @RequestParam("status") String status) {
		ResponseEntity<Object> responseEntity = businessValidator.validOrderIdandStatus(id, status);
		if (responseEntity == null) {
			try {
				orderservice.cancelOrder(id, status);
				return new ResponseEntity<Object>(new ResponseModel(true, AppConstant.ORDER_Cancled_STRING, null, 1),
						HttpStatus.OK);
			} catch (Exception e) {
				return new ResponseEntity<Object>(new ResponseModel(false, e.getMessage(), null, 0),
						HttpStatus.INTERNAL_SERVER_ERROR);
			}
		} else {
			return responseEntity;

		}
	}
}
