package com.example.demo.validator;

import org.springframework.http.ResponseEntity;

import com.example.demo.entity.CartProductEntity;
import com.example.demo.entity.ProductEntity;
import com.example.demo.entity.UserEntity;



public interface BusinessValidator {
	public ResponseEntity<Object> validUserDetails(UserEntity userModel);
	
	public ResponseEntity<Object> validProductDetail(ProductEntity productEntity);
	public ResponseEntity<Object> validCartDetail(CartProductEntity cartEntity);
	public ResponseEntity<Object> validUserId(String userid);
	public ResponseEntity<Object> validCartId(String cartid);
	public ResponseEntity<Object> validOrderId(String orderid);
	public ResponseEntity<Object> validOrderIdandStatus(String orderid,String status);
	
	public ResponseEntity<Object> validOrderIdProductidUseridandquanity(String orderid,String productid,String userid,String quantity);
	
}
