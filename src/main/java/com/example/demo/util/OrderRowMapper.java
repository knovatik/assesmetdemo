package com.example.demo.util;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.example.demo.entity.OrderDataEntity;

public class OrderRowMapper implements RowMapper<OrderDataEntity> {

	@Override
	public OrderDataEntity mapRow(ResultSet rs, int rowNum) throws SQLException {
		// TODO Auto-generated method stub
		
		OrderDataEntity orderDataEntity=new OrderDataEntity();
		
		orderDataEntity.setId(rs.getLong("PRODUCT_ID"));
		orderDataEntity.setProductname(rs.getString("PRODUCTNAME"));
		orderDataEntity.setPrice(rs.getDouble("PRICE"));
		orderDataEntity.setProductquantity(rs.getDouble("PRODUCTQUANTITY"));

		return orderDataEntity;
	}

}
