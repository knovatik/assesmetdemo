
package com.example.demo.util;

/**
 * @author Pawan Tripathi
 *
 */
public class AppConstant {
	public static final String USER_NOT_FOUND = "User Not Found";
	public static final String ENTER_CORRECT_EMAIL = "Please enter Correct email";
	public static final String USER_REGISTRATION_SUCCESSFULLY = "User registration successful";
	public static final String User_NAME_EMPTY = "Please Enter Name";
	public static final String User_Address_EMPTY = "Please Enter Address";
	public static final String User_ID_EMPTY = "Please Enter UserId";
	public static final String Order_ID_EMPTY = "Please Enter OrderId";
	public static final String Order_Status_EMPTY = "Please Enter OrderStatus";
	public static final String Product_Responce = "Product Added Succesfully";
	public static final String Product_Cart_Responce = "Product Added to cart Succesfully";
	public static final String Product_Name_Emptiy = "Please Enter Produtname";
	public static final String Product_Price_Emptiy = "Please Enter ProductPrice";
	public static final String Product_ID_Emptiy = "Please Enter Productid";
	public static final String Product_Quantiy_Emptiy = "Please Enter Product Quantity";
	public static final String Cart_Name_Emptiy = "Please Enter Produtname";
	public static final String Cart_Price_Emptiy = "Please Enter ProductPrice";
	public static final String Cart_Quantiy_Emptiy = "Please Enter Product Quantity";
	public static final String Cart_id_Emptiy = "Please Enter Cart Id";
	public static final String Addres_Updated_Succesfully = "Address Updated Succesfully";
	public static final String Quantity_Updated_Succesfully = "Quantity Updated Successfully";
	public static final String ORDER_SUCCES_STRING = "Order placed succesfully";
	public static final String ORDER_STATUS_STRING = "Order status updated succesfully";
	public static final String GET_ORDER_SUCCES_STRING ="succesfully";
	public static final String ORDER_Cancled_STRING = "Order Canceled  Successfully";
	
	}
