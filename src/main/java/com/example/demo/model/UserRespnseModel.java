package com.example.demo.model;

import com.example.demo.entity.UserEntity;

/**Create Model class
 * Set details from API
 * @author SushilY
 *
 */
public class UserRespnseModel {

	private Boolean status;
	private String message;
	private UserEntity userEntity;
	
	

	/**
	 * @return
	 */
	public Boolean getStatus() {
		return status;
	}

	/**
	 * @param status
	 */
	public void setStatus(Boolean status) {
		this.status = status;
	}

	/**
	 * @return
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message
	 */
	public void setMessage(String message) {
		this.message = message;
	}
	/**
	 * @param status
	 * @param message
	 */
	public UserRespnseModel(Boolean status,UserEntity userEntity, String message) {
		super();
		this.status = status;
		this.message = message;
	}
}
