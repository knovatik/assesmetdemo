package com.example.demo.model;

import com.example.demo.entity.ProductEntity;

public class ProductResponceModel {
	
	private Boolean status;
	private String message;
	/**
	 * @return
	 */
	public Boolean getStatus() {
		return status;
	}

	/**
	 * @param status
	 */
	public void setStatus(Boolean status) {
		this.status = status;
	}

	/**
	 * @return
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message
	 */
	public void setMessage(String message) {
		this.message = message;
	}
	/**
	 * @param status
	 * @param message
	 */
	public ProductResponceModel(Boolean status,ProductEntity productEntity, String message) {
		super();
		this.status = status;
		this.message = message;
	}

}
