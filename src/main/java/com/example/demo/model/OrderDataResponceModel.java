package com.example.demo.model;

import java.util.List;

import com.example.demo.entity.OrderDataEntity;

public class OrderDataResponceModel {
	private Boolean status;
	private String message;
	List<OrderDataEntity> orderDataEntities;
	
	

	/**
	 * @return
	 */
	public Boolean getStatus() {
		return status;
	}

	/**
	 * @param status
	 */
	public void setStatus(Boolean status) {
		this.status = status;
	}

	/**
	 * @return
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message
	 */
	public void setMessage(String message) {
		this.message = message;
	}
	/**
	 * @param status
	 * @param message
	 */
	
	
	
	public OrderDataResponceModel(Boolean status,List<OrderDataEntity>  oderDataEntities, String message) {
		this.status = status;
		this.message = message;
		this.orderDataEntities=oderDataEntities;
	}

	public List<OrderDataEntity> getOrderDataEntities() {
		return orderDataEntities;
	}

	public void setOrderDataEntities(List<OrderDataEntity> orderDataEntities) {
		this.orderDataEntities = orderDataEntities;
	}
}
